package yandex.caching;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Simple cache implementation (with LRU and FIFO strategies) based on {@link LinkedHashMap}
 * Rehashing of underlying {@link java.util.HashMap} is impossible
 * Use one of factory-methods to create instance of cache.
 */
public class SimpleCache<K, V> extends LinkedHashMap<K, V> {

    public enum AlgorithmType {
        LRU, FIFO
    }

    private static final long serialVersionUID = -3801528242820542131L;

    /**
     * Default maximum size of cache.
     */
    private static final int DEFAULT_MAX_SIZE = 1 << 10;

    /**
     * The maximum size, used if a higher value is implicitly specified in constructor.
     */
    private static final int ABSOLUTE_MAX_SIZE = 1 << 30;

    /**
     * The load factor used for {@link SimpleCache}.
     */
    private static final float DEFAULT_LOAD_FACTOR = 1.0f;

    private int maxSize;

    /**
     * Constructs a new empty SimpleCache with given max size.
     * @param maxSize max size of SimpleCache
     *                (max number of key-value pairs, that can be in SimpleCache in the single period of time)
     * @param useLRU flag indicating whether to use LRU algorithm or FIFO algorithm
     */
    private SimpleCache(int maxSize, boolean useLRU) {
        super(maxSize, DEFAULT_LOAD_FACTOR, useLRU);

        this.maxSize = maxSize;
    }

    /**
     * Returns a new empty SimpleCache with default max size and given cache algorithm {@link AlgorithmType}.
     * @param algorithmType {@link yandex.caching.SimpleCache.AlgorithmType} cache algorithm
     */
    public static <K, V> SimpleCache<K, V> createCache(AlgorithmType algorithmType) {
        return createCache(DEFAULT_MAX_SIZE, algorithmType);
    }

    /**
     * Returns a new empty SimpleCache with given max size and given cache algorithm {@link AlgorithmType}.
     * @param maxSize maxSize of underlying {@link java.util.HashMap}
     * @param algorithmType {@link yandex.caching.SimpleCache.AlgorithmType} cache algorithm
     */
    public static <K, V> SimpleCache<K, V> createCache(int maxSize, AlgorithmType algorithmType) {
        if (maxSize < 0) {
            throw new IllegalArgumentException("Illegal max size: " + maxSize);
        }

        if (maxSize > ABSOLUTE_MAX_SIZE) {
            maxSize = ABSOLUTE_MAX_SIZE;
        }

        return new SimpleCache<K, V>(maxSize, algorithmType == AlgorithmType.LRU);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > maxSize;
    }
}
