package yandex.caching;

import junit.framework.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

public class SimpleCacheTest {

    @Test(expected = IllegalArgumentException.class)
    public void testWrongMaxSize() {
        SimpleCache.createCache(-1, SimpleCache.AlgorithmType.FIFO);
    }

    /**
     * Tests impossibility of rehashing
     */
    @Test
    public void testInitialization() throws IllegalAccessException, NoSuchFieldException {
        SimpleCache<Integer, Object> cache = SimpleCache.createCache(1 << 20, SimpleCache.AlgorithmType.FIFO);

        Field thresholdField = cache.getClass().getSuperclass().getSuperclass().getDeclaredField("threshold");
        thresholdField.setAccessible(true);
        int threshold = thresholdField.getInt(cache);

        Field maxSizeField = cache.getClass().getDeclaredField("maxSize");
        maxSizeField.setAccessible(true);
        int max_size = maxSizeField.getInt(cache);

        Assert.assertEquals(threshold, max_size);
    }

    @Test
    public void testFIFOCorrectness() {
        int cacheSize = 5;
        SimpleCache<Integer, Integer> cache = SimpleCache.createCache(cacheSize, SimpleCache.AlgorithmType.FIFO);
        for (int i = 0; i < cacheSize; i++) {
            cache.put(i, i * 2);
        }

        for (int i = 0; i < cacheSize; i++) {
            Integer value = cache.get(i);
            Assert.assertNotNull(value);
            Assert.assertEquals(i * 2, value.intValue());
        }

        cache.put(6, 12);
        cache.put(7, 14);

        Assert.assertEquals(cacheSize, cache.size());

        Assert.assertNull(cache.get(0));
        Assert.assertNull(cache.get(1));

        Assert.assertEquals(4, cache.get(2).intValue());
        Assert.assertEquals(6, cache.get(3).intValue());
        Assert.assertEquals(8, cache.get(4).intValue());
        Assert.assertEquals(12, cache.get(6).intValue());
        Assert.assertEquals(14, cache.get(7).intValue());
    }

    @Test
    public void testLRUCorrectness() {
        int cacheSize = 5;
        SimpleCache<Integer, Integer> cache = SimpleCache.createCache(cacheSize, SimpleCache.AlgorithmType.LRU);
        for (int i = 0; i < cacheSize; i++) {
            cache.put(i, i * 2);
        }

        for (int i = 0; i < cacheSize; i++) {
            Integer value = cache.get(i);
            Assert.assertNotNull(value);
            Assert.assertEquals(i * 2, value.intValue());
        }

        cache.get(0);
        cache.get(2);
        cache.put(6, 12);
        cache.put(7, 14);

        Assert.assertEquals(cacheSize, cache.size());

        Assert.assertNull(cache.get(1));
        Assert.assertNull(cache.get(3));

        Assert.assertEquals(0, cache.get(0).intValue());
        Assert.assertEquals(4, cache.get(2).intValue());
        Assert.assertEquals(8, cache.get(4).intValue());
        Assert.assertEquals(12, cache.get(6).intValue());
        Assert.assertEquals(14, cache.get(7).intValue());
    }
}
